from Tkinter import *

window=Tk()
window.title("Monitor Automation")
window.minsize(width = 600, height = 300)
window.resizable(False, False)

loginLabel=Label(window,text='Login',fg='red',font='arial 14').place(x = 150, y = 20, width = 60, height=30)
passwordLabel=Label(window,text='Password',fg='red',font='arial 14').place(x = 150, y = 50, width = 100, height=30)

login=Text(window,font='Arial 14').place(x = 280, y = 20, width = 120, height=30)
password=Text(window,font='Arial 14')
password=Entry(window,textvariable=password, show="*").place(x = 280, y = 50, width = 120, height=30)

var1=IntVar()

group_0=Checkbutton(window,text=u'group 0',variable=var1,onvalue=1,offvalue=0).place(x = 150, y = 80, width = 70, height=30)
group_1=Checkbutton(window,text=u'group 1',variable=var1,onvalue=1,offvalue=0).place(x = 220, y = 80, width = 70, height=30)
group_2=Checkbutton(window,text=u'group 2',variable=var1,onvalue=1,offvalue=0).place(x = 290, y = 80, width = 70, height=30)
group_3=Checkbutton(window,text=u'group 3',variable=var1,onvalue=1,offvalue=0).place(x = 360, y = 80, width = 70, height=30)
group_11=Checkbutton(window,text=u'group 11',variable=var1,onvalue=1,offvalue=0).place(x = 150, y = 110, width = 80, height=30)
group_12=Checkbutton(window,text=u'group 12',variable=var1,onvalue=1,offvalue=0).place(x = 230, y = 110, width = 80, height=30)
group_13=Checkbutton(window,text=u'group 13',variable=var1,onvalue=1,offvalue=0).place(x = 310, y = 110, width = 80, height=30)
group_14=Checkbutton(window,text=u'group 14',variable=var1,onvalue=1,offvalue=0).place(x = 390, y = 110, width = 80, height=30)
group_21=Checkbutton(window,text=u'group 21',variable=var1,onvalue=1,offvalue=0).place(x = 150, y = 140, width = 80, height=30)
group_22=Checkbutton(window,text=u'group 22',variable=var1,onvalue=1,offvalue=0).place(x = 230, y = 140, width = 80, height=30)
group_23=Checkbutton(window,text=u'group 23',variable=var1,onvalue=1,offvalue=0).place(x = 310, y = 140, width = 80, height=30)

button1=Button(window,text='Process',fg='Black',font='arial 14').place(x = 270, y = 180, width = 80, height=30)

window.mainloop()