DESCRIPTION:
This script allows you to process lamps (ignore or wait) from monitor v1.

ADDITIONAL INSTALLATION:
Before using this script it is needed to install extra libraries (firstly you should install pip package):
sudo pip install selenium
sudo pip install python-rtkit

CONTENT:
This repository contains the following 3 files:
1. gui.py - responsible for GUI part of the script (currently unavailable).
2. login.py - main file that should be run (contains the login).
3. rt.py - responsible for retrieving information (status of the ticket) from RequestTracker.

USAGE:
> python login.py -h
usage: login.py [-h] 
                [-s] 
                [-g SUPPORTGROUP] 
                [--mTime MTIME|--nTime NTIME]
                [-o {C,S,K}]
                username

positional arguments:
  username              login of RT system

optional arguments:
  -h, --help            show this help message and exit
  -s, --ignoreHosts     ignore the whole host
  -g SUPPORTGROUP, --supportGroup SUPPORTGROUP
                        the support group to process
  --mTime MTIME         the morning time until which to ignore lamps
  --nTime NTIME         the night time until which to ignore lamps
  -o {C,S,K}, --office {C,S,K}
                        the company office

Examples:
1.You want to ignore not only lamps and the hosts (no update) also for your office:
python login.py <nickname> -s -o=C

2. You do not have to ignore whole hosts and want to use specific group:
python login.py <nickname> -g=21

3. You want to ignore lamps to the specific time (by default it ignores till the end of the current shift (8 a.m. or 8 p.m.)):
python login.py <nickname> -o=S --mTime="09:00:00"

Than, script asks you to enter the password.

PLEASE NOTE, that if you use password+OTP on RT, you should also enter 6 digits (you may use any 6 digits as this feature doesn't work for now).

LOGIC:
The script checks all "red" lamps and according to the following criteria decides what should we do with this lamp:
- If field "Status Changed" more than 2 days
- If in the comments the lamp is mentioned and there is a ticket

script will ignore or wait this lamp depends on the last action (from Comments).

