import logging
from rtkit import set_logging
from rtkit.authenticators import CookieAuthenticator
from rtkit.errors import RTResourceError
from rtkit.resource import RTResource

set_logging('info')
logger = logging.getLogger('rtkit')


class RequestTracker:
    # here is the RequestTracker URI we try to access
    host_uri = 'https://rt.portaone.com/REST/1.0/'

    def __init__(self, user, password):
        self.user = user
        self.password = password

    def check_status(self, ticket):
        """Returns the status of the ticket"""
        resource = RTResource(self.host_uri, self.user, self.password, CookieAuthenticator)
        try:
            response = resource.post(path='ticket/'+str(ticket))
            if response.parsed:
                parse_status = [element for element in response.parsed[0] if element[0] == 'Status']
                if parse_status[0]:
                    (name, status) = parse_status[0]
                    return status
        except RTResourceError as e:
            logger.error(e.response.status_int)
            logger.error(e.response.status)
            logger.error(e.response.parsed)
