import argparse
import getpass
from datetime import datetime, timedelta
from time import sleep
from selenium import webdriver
from rt import RequestTracker

# variables
content_list = []
otp = ''
otp_users = [
    #Sumy
    "ant",
    "gomer",
    "ice17",
    "osama",
    "orel",
    #Chernigov
    "shc",
    "mpyou",
    "admiral",
    "dgeremi",
    "icgin"
]

# list of all lamps
critical_names_list = [
    "Apache",
    "Avalara",
    "Archivist",
    "BE Log Index",
    "Billsoft",
    "Call Recording",
    "CDR Extractor",
    "CDR Rating",
    "Cassandra",
    "Certificate",
    "Configurator Agent",
    "Coredump",
    "CPU Usage",
    "Crontab",
    "Current Load",
    "Disconnector",
    "Disk Health",
    "Disk Load",
    "Disk Space",
    "DNS Response",
    "Edge Proxy",
    "Elasticsearch",
    "Exim Mail Queue",
    "High Availability Cluster",
    "IMAP",
    "IMGate",
    "Gearman",
    "License",
    "Logserver",
    "Logmaster",
    "Memory",
    "MFd",
    "MUBundle",
    "MWId",
    "MWI Statistics",
    "Mysql",
    "MySQL Deadlock",
    "Network",
    "Network Interface",
    "NTP",
    "Pacemaker",
    "Ping",
    "PNC",
    "Registrar",
    "Subscription manager",
    "SipSelfCheck",
    "Sysctl",
    "System Network",
    "Tasks Stack",
    "TCP Statistics",
    "TLS certificate",
    "Virtual Memory IO",
    "Watchdog",
    "Zombie",
]

whole_date = datetime.today()
current_date = whole_date.strftime("%Y-%m-%d")
current_time = whole_date.strftime("%H:%M:%S")

# constants
morning_shift_time = "08:00:00"
morning_time_custom = "08:00:00"
evening_shift_time = "20:00:00"
evening_time_custom = "20:00:00"

# parsing cmd arguments
parser = argparse.ArgumentParser()
parser.add_argument('username', type=str, nargs=1, help="login of RT system")
parser.add_argument("-s", "--ignoreHosts", help="ignore the whole host", action="store_true")
parser.add_argument("-g", "--supportGroup", nargs=1, help="the support group to process", type=int)
parser.add_argument("--mTime", nargs=1, help="the morning time until which to ignore lamps", type=str)
parser.add_argument("--nTime", nargs=1, help="the night time until which to ignore lamps", type=str)
parser.add_argument("-o", "--office", help="the company office", type=str, choices=["C", "S", "K"])
args = parser.parse_args()


def check_otp_user(user):
    """Checks if user belongs to ones with OTP enabled"""
    return user in otp_users


def generate_password(user, passwd):
    """Generates password for user"""
    if check_otp_user(user):
        pswd = passwd[-6:]
        return passwd[:-6], pswd
    else:
        return passwd


def get_content(wdriver, alert):
    """Retrieves the user's comment from the lamp history"""
    k = 0
    action_labels = ['Ignore', 'Wait']
    for inner_content in wdriver:
        # get actions list
        actions = inner_content.find_elements_by_css_selector("dt.do_highlight > span.caction")
        # get comment body
        contexts = inner_content.find_elements_by_css_selector("dd.do_highlight > pre")

        for action in actions:
            if action.text not in action_labels:
                k = k + 1
            else:
                try:
                    if contexts[k].text.find(alert + ":") != -1 and actions[k].text == 'Ignore':
                        return actions[k].text, contexts[k].text.splitlines()[2]
                    if contexts[k].text.find(alert + ":") != -1 and actions[k].text == 'Wait':
                        return actions[k].text, contexts[k].text.splitlines()[1]
                except IndexError:
                    return []


def ignore_action(wdriver, k):
    if current_time < morning_shift_time:
        # date
        wdriver.clear()
        wdriver.send_keys(current_date)
        popup = browser.find_element_by_id("ui-datepicker-div")
        browser.execute_script('arguments[0].style = "display: none;"', popup)
        sleep(1)
        browser.find_elements_by_css_selector('div.leftAlign.date_input > input.time')[k].clear()
        # time
        browser.find_elements_by_css_selector('div.leftAlign.date_input > input.time')[k].send_keys(morning_time_custom)
    elif current_time > evening_shift_time:
        # date
        wdriver.clear()
        wdriver.send_keys((datetime.today() + timedelta(days=1)).strftime("%Y-%m-%d"))
        popup = browser.find_element_by_id("ui-datepicker-div")
        browser.execute_script('arguments[0].style = "display: none;"', popup)
        sleep(1)
        browser.find_elements_by_css_selector('div.leftAlign.date_input > input.time')[k].clear()
        # time
        browser.find_elements_by_css_selector('div.leftAlign.date_input > input.time')[k].send_keys(morning_time_custom)
    else:
        # date
        wdriver.clear()
        wdriver.send_keys(current_date)
        popup = browser.find_element_by_id("ui-datepicker-div")
        browser.execute_script('arguments[0].style = "display: none;"', popup)
        sleep(1)
        browser.find_elements_by_css_selector('div.leftAlign.date_input > input.time')[k].clear()
        # time
        browser.find_elements_by_css_selector('div.leftAlign.date_input > input.time')[k].send_keys(evening_time_custom)


def should_process_lamp(last_changed_status, curr_time):
    diff = curr_time - datetime.strptime(last_changed_status, "%Y-%m-%d")
    if diff.days > 2:
        return True
    else:
        return False


def process_lamp(k, action, message):
    browser.find_elements_by_css_selector('div.info_block > div.action_block > input#take')[k].click()
    if browser.find_elements_by_css_selector('div.action_comment_cont > div')[k].find_element_by_name(
            'done').is_displayed():
        browser.find_elements_by_css_selector('div.action_comment_cont > div')[k].find_element_by_name('done').click()

        # check if comment contains "Ignored" word
        if action.find("Ignore") != -1:
            browser.find_elements_by_css_selector('div.action_block')[k].find_element_by_id('ignore').click()
            if k == 1:
                ignore_action(
                    browser.find_element_by_css_selector('div.leftAlign.date_input > input.date.hasDatepicker'), k)
            else:
                ignore_action(
                    browser.find_element_by_css_selector('div.leftAlign.date_input > label > input.date.hasDatepicker'),
                    k)
        # check if comment contains "Wait" word
        elif action.find("Wait") != -1:
            browser.find_element_by_css_selector('div.action_block').find_element_by_id('wait').click()
        else:
            browser.find_elements_by_css_selector('div.action_block')[k].find_element_by_name('cancel').click()

        # comment
        browser.find_elements_by_css_selector('div.action_comment_cont > textarea.action_comment')[k].send_keys(message)

        browser.find_elements_by_css_selector('div.action_comment_cont > div')[k].find_element_by_name('done').click()
    else:
        browser.find_elements_by_css_selector('div.action_block')[k].find_element_by_name('cancel').click()


def ignore_server(wdriver):
    """Ignores the whole server if the number of lamps on it is >= 9"""
    number_of_critical_lamps = []
    if wdriver and wdriver.is_enabled and wdriver.is_displayed:
        wdriver.click()
        sleep(1)
        if len(browser.find_elements_by_css_selector('div.notes_container.comments > dl > dd')) > 1:
            content = browser.find_elements_by_css_selector('div.notes_container.comments > dl > dd')[
                1].find_elements_by_css_selector('div.cserv > p.alert')
        else:
            return
        for alarms in content:
            for alarm in alarms.find_elements_by_css_selector('span.dyn_status'):
                number_of_critical_lamps.append(alarm.text)

        if len(number_of_critical_lamps) >= 9:
            sleep(1)
            message = browser.find_element_by_css_selector(
                'div.notes_block > div.notes_container.comments > dl > dd > pre').text
            process_lamp(1, "Ignore", message.splitlines()[1:])
        else:
            wdriver.click()


def check_ticket_number(message):
    """Checks the ticket status in RT"""
    status = []
    tickets = [int(my_id) for my_id in message.split() if my_id.isdigit()]
    for ticket in tickets:
        status.append(rt.check_status(ticket))

    is_active = all(stat != 'closed' and stat != 'resolved' and stat != '' for stat in status)
    if is_active:
        return True
    else:
        return False


def validate_time(time):
    """Validates the time according to the format %H:%M:%S"""
    time_format = "%H:%M:%S"
    user_time = time[0]
    print(user_time)
    try:
        valid_time = datetime.strptime(user_time, time_format)
        return user_time
    except ValueError:
        print("Time was entered in wrong format.\nYour value - {0}\nCorrect format - %H:%M:%S".format(user_time))
        exit(1)


# enter credentials
login = ''
if args.username:
    login = args.username[0]
else:
    exit('Username missing')
password = getpass.getpass()

# check custom time for ignoring lamps
if args.mTime:
    morning_time_custom = validate_time(args.mTime)

if args.nTime:
    evening_time_custom = validate_time(args.nTime)

# process otp user
if check_otp_user(login):
    password = generate_password(login, password)[0]
    otp = generate_password(login, password)[1]
    rt = RequestTracker(login, password + otp)
else:
    rt = RequestTracker(login, password)

# open browser window and log in to monitor
browser = webdriver.Firefox()
browser.get('https://monitor2.portaone.com')
email = browser.find_element_by_id('pb_auth_user')
email.send_keys(login)
pwd = browser.find_element_by_id('pb_auth_password')
pwd.send_keys(password)

browser.find_element_by_xpath('/html/body/center/form/table/tbody/tr[3]/td[2]/input').click()

# Select support group to process
if args.supportGroup:
    group = str(args.supportGroup[0])
    xpath = '//*[@id="support_group_{0}"]'.format(group)
    browser.find_element_by_xpath(xpath).click()
elif args.office == 'S':
    browser.find_element_by_xpath('//*[@id="support_group_21"]').click()
    browser.find_element_by_xpath('//*[@id="support_group_22"]').click()
    browser.find_element_by_xpath('//*[@id="support_group_23"]').click()
elif args.office == 'C':
    browser.find_element_by_xpath('//*[@id="support_group_11"]').click()
    browser.find_element_by_xpath('//*[@id="support_group_12"]').click()
    browser.find_element_by_xpath('//*[@id="support_group_13"]').click()
    browser.find_element_by_xpath('//*[@id="support_group_14"]').click()
elif args.office == 'K':
    browser.find_element_by_xpath('//*[@id="support_group_0"]').click()
    browser.find_element_by_xpath('//*[@id="support_group_1"]').click()
    browser.find_element_by_xpath('//*[@id="support_group_2"]').click()
    browser.find_element_by_xpath('//*[@id="support_group_3"]').click()

browser.find_element_by_xpath('//*[@id="apply_but"]').submit()
sleep(5)

# Apply filters
browser.find_element_by_id('filter_1').click()

# Expand all critical lamps
for i in browser.find_elements_by_css_selector('li.alert > button.expand_btn'):
    i.click()
sleep(5)

server_elements = browser.find_elements_by_css_selector('ul:not([class^="p"]) > li[class^="host alert"] > a')

if args.ignoreHosts:
    # process hosts, if host is in no update state (there are more then 9 alerts on it, ignore the whole host)
    for server in server_elements:
        ignore_server(server)

elements = browser.find_elements_by_css_selector('ul:not([class^="h"]) > li[class$="service alert"] > a')

# process individual lamps
for element in elements:
    if element and element.is_enabled and element.is_displayed:
        for alarm_name in critical_names_list:
            if element.text.find(alarm_name) > 0:
                element.click()
                sleep(2)
                shouldProcess = should_process_lamp(browser.find_element_by_css_selector(
                    'table.info_table > tbody > tr > td.value.value_text.last_change').text.split(',')[0].split(' ')[0],
                                                    whole_date)
                if shouldProcess:
                    content_list = get_content(browser.find_elements_by_css_selector
                                               ('div.notes_container.comments > dl'), alarm_name)
                    if content_list:
                        if check_ticket_number(content_list[1]):
                            process_lamp(0, content_list[0], content_list[1])
                        content_list = []
    content_list = []
browser.quit()
